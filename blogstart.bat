
rem 打开一个新窗口并执行连接mongodb数据库至当前文件夹
start cmd.exe /k  "mongod -dbpath %cd%"


rem 人工等待连接成功
echo 若mongodb连接成功，下一步开启服务器
@pause


start cmd.exe /k  "node-dev ./bin/www"

rem 人工等待服务器开启
echo 若服务器开启，下一步监听html,css,js,json,ejs（以下步骤非必需，浏览器内输入localhost:3000即可）
@pause
echo 监听开启中...
browser-sync start --proxy "localhost:3000" --files "public/**, views/**, models/**, routes/**"
