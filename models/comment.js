var mongodb = require('./db');
var ObjectId = require('mongodb').ObjectID;

function Comment(author, day, post_id, comment) {
	this.author = author;
	this.day = day;
	this.post_id = post_id;
	this.comment = comment;
}

module.exports = Comment;

Comment.prototype.save = function (callback) {
	var author = this.author,
		day = this.day,
		post_id = this.post_id,
		comment = this.comment;

	mongodb.open(function (err, db) {
		if (err) {
			return callback(err);
		}
		db.collection('posts', function (err, collection) {
			if (err) {
				mongodb.close();
				return callback(err);
			}
			collection.update({
				"author" : author,
				"time.day": day,
				"_id": ObjectId(post_id)
			}, {
				$push: {"comments": comment}
			}, function (err) {
				mongodb.close();
				if (err) {
					return callback(err);
				}
				callback(null, comment);
			});
		});
	});
};
