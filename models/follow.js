var mongodb = require('./db');

function Follow(username, follow) {
	this.username = username;
	this.follow = follow;
}

module.exports = Follow;

Follow.prototype.save = function (callback) {
	var username = this.username,
		follow = this.follow;

	mongodb.open(function (err, db) {
		if (err) {
			return callback(err);
		}
		db.collection('users', function (err, collection) {
			if (err) {
				mongodb.close();
				return callback(err);
			}
			collection.update({
				"username" : username
			}, {
				$push: {"follows": follow}
			}, function (err) {
				mongodb.close();
				if (err) {
					return callback(err);
				}
				callback(null, follow);
			});
		});
	});
};

Follow.remove = function (username, fusername, callback) {
	mongodb.open(function (err, db) {
		if (err) {
			return callback(err);
		}
		db.collection('users', function (err, collection) {
			if (err) {
				mongodb.close();
				return callback(err);
			}	
			collection.update({
				"username" : username
			}, {
				$pull: {
					"follows": {
						"username":fusername
					}
				}
			}, function (err) {
				mongodb.close();
				if (err) {
					return callback(err);
				}
				callback(null);
			});
		});
	});
};

Follow.getAll = function (username, callback) {
	mongodb.open(function (err, db) {
		if (err) {
			return callback(err);
		}
		db.collection('users', function (err, collection){
			if (err) {
				mongodb.close();
				return callback(err);
			}
			var query = {};
			if (username) {
				query.username = username;
			}
			collection.find(query).sort({time: -1}).toArray(function (err, follows) {
				mongodb.close();
				if (err) {
					return callback(err);
				}
				callback(null, follows);
			});
		});
	});
};
