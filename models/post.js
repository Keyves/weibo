var mongodb = require('./db');
var ObjectId = require('mongodb').ObjectID;
var markdown = require('markdown').markdown;

function Post(author, content, image, refer) {
	this.author = author;
	this.content = content;
	this.image = image;
	this.refer = refer;
}

module.exports = Post;

Post.prototype.save = function (callback) {
	var date = new Date();
	var time = {
		date : date,
		year : date.getFullYear(),
		month : date.getFullYear() + "-" + (date.getMonth() + 1),
		day : date.getFullYear() + "-" + (date.getMonth() + 1) + "-" +date.getDate(),
		minute : date.getFullYear() + "-" + (date.getMonth() + 1) + "-" +date.getDate() + " " +date.getHours() + ":" + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes())
	};
	var post = {
		author : this.author,
		time : time,
		content : this.content,
		refer : this.refer,
		comments : []
	};
	mongodb.open(function (err, db) {
		if (err) {
			return callback(err);
		}
		db.collection('posts', function (err, collection) {
			if (err) {
				mongodb.close();
				return callback(err);
			}
			collection.insert(post, {safe: true}, function (err) {
				mongodb.close();
				if (err) {
					return callback(err);
				}
				//post.content = markdown.toHTML(post.content);
				callback(null, post);
			});
		});
	});
};

Post.getAll = function (author, callback) {
	mongodb.open(function (err, db) {
		if (err) {
			return callback(err);
		}
		db.collection('posts', function (err, collection){
			if (err) {
				mongodb.close();
				return callback(err);
			}
			var query = {};
			if (author) {
				query.author = author;
			}
			collection.find(query).sort({time: -1}).toArray(function (err, docs) {
				mongodb.close();
				if (err) {
					return callback(err);
				}
				docs.forEach(function (doc){
					//doc.content = markdown.toHTML(doc.content);
				});
				callback(null, docs);
			});
		});
	});
};
Post.getOne = function (author, day, post_id, callback) {
	mongodb.open(function (err, db) {
		if (err) {
			return callback(err);
		}
		db.collection('posts', function (err, collection) {
			if (err) {
				mongodb.close();
				return callback(err);
			}
			collection.findOne({"author": author, "time.day": day, "_id": ObjectId(post_id)}, function (err, doc) {
				mongodb.close();
				if (err) {
					return callback(err);
				}
				if(doc) {
					//doc.content = markdown.toHTML(doc.content);
				}
				callback(null, doc);
			});
		});
	});
};
Post.edit = function (author, day, post_id, callback) {
	mongodb.open(function (err, db) {
		if (err) {
			return callback(err);
		}
		db.collection('posts', function (err, collection) {
			if (err) {
				mongodb.close();
				return callback(err);
			}
			collection.findOne({"author": author, "time.day": day, "_id": ObjectId(post_id)}, function (err, doc) {
				mongodb.close();
				if (err) {
					return callback(err);
				}
				callback(null, doc);
			});
		});
	});
};
Post.update = function (author, day, post_id, content, callback) {
	mongodb.open(function (err, db) {
		if (err) {
			return callback(err);
		}
		db.collection('posts', function (err, collection) {
			if (err) {
				mongodb.close();
				return callback(err);
			}
			collection.update({"author": author, "time.day": day, "_id": ObjectId(post_id)}, {$set: {content: content}}, function (err) {
				mongodb.close();
				if (err) {
					return callback(err);
				}
				callback(null);
			});
		});
	});
};

Post.remove = function (author, day, post_id, callback) {
	mongodb.open(function (err, db) {
		if (err) {
			return callback(err);
		}
		db.collection('posts', function (err, collection) {
			if (err) {
				mongodb.close();
				return callback(err);
			}	
			collection.remove({"author": author, "time.day": day, "_id": ObjectId(post_id)}, {w: 1}, function (err) {
				mongodb.close();
				if (err) {
					return callback(err);
				}
				callback(null);
			});
		});
	});
};

