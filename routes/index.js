var express = require('express');
var ejs = require('ejs');
var path = require('path');
var router = express.Router();
var crypto = require('crypto');
var fs = require('fs');
var User = require('../models/user.js');
var Post = require('../models/post.js');
var Comment = require('../models/comment.js');
var Follow = require('../models/follow.js');

/* GET home page. */
router.get('/', function (req, res, next) {
	Post.getAll(null, function (err, posts) {
		if (err) {
			posts = [];
		}
	  	res.render('index', { 
	  		title: '微博',
	  		user: req.session.user,
	  		interviewee: req.session.user,
	  		posts: posts,
	  		layout:false,
	  		success: req.flash('success').toString(),
	  		error: req.flash('error').toString()
	  	});
	});
});


/***********************用户主页***************/
router.get('/u/:username', function (req, res, next) {
	User.getOne(req.params.username, function (err, interviewee) {
		if (!interviewee) {
			req.flash('error', 'interviewee does not exist!');
			return res.redirect('/');
		}
		Post.getAll(interviewee.username, function (err, posts) {
			if (err) {
				req.flash('error', err);
				return res.redirect('/');
			}
			var currentUser = req.session.user
			,	followed = false;
			if (currentUser) {
				User.getOne(currentUser.username, function (err, user) {
					if (!user) {
						req.flash('error', 'user does not exist!');
						return res.redirect('/');
					}
					currentUser = user;
					currentUser.follows.forEach(function (follow) {
						if (follow.username === interviewee.username)
							return followed = true;
					});
					res.render('user', {
						interviewee: interviewee,
						posts: posts,
						followed: followed,
						user: currentUser,
						success: req.flash('success').toString(),
						error: req.flash('error').toString()
					});
				});
			}
			else {
				res.render('user', {
					interviewee: interviewee,
					posts: posts,
					followed: followed,
					user: currentUser,
					success: req.flash('success').toString(),
					error: req.flash('error').toString()
				});
			}
		});
	});
});

/***********************注册***************/
router.get('/reg', checkNotLogin);
router.get('/reg', function (req, res, next) {
	res.render('reg', { 
		title:'register',
  		user: req.session.user,
  		success: req.flash('success').toString(),
  		error: req.flash('error').toString()
	});
});

router.post('/reg', checkNotLogin);
router.post('/reg', function (req, res, next) {
	var username = req.body.username,
		password = req.body.password,
		password_re = req.body['password-rep'];

	if(password != password_re){
		req.flash('error', '两次输入的密码不一致！');
		return res.redirect('back');
	}
	var md5 = crypto.createHash('md5'),
		password = md5.update(req.body.password).digest('hex');
	var newUser = new User({
		username: req.body.username,
		password: password,
		email: req.body.email
	});
	User.getOne(newUser.username, function (err, user) {
		if (user) {
			req.flash('error','user has existed!');
			return res.redirect('back');
		}
		newUser.save(function (err, user) {
			if (err) {
				req.flash('error', err);
				return res.redirect('back');
			}
			req.session.user = user;
			req.flash('success', 'register successfully!');
			res.redirect('/');
		});
	});
});

/***********************登录***************/
router.get('/login', checkNotLogin);
router.get('/login', function (req, res, next) {
	res.render('login', { 
		title:'login',
  		user: req.session.user,
  		success: req.flash('success').toString(),
  		error: req.flash('error').toString()
  	});
});

router.post('/login', checkNotLogin);
router.post('/login', function (req, res, next) {	
	var md5 = crypto.createHash('md5'),
		password = md5.update(req.body.password).digest('hex');
	var newUser = new User({
		username: req.body.username,
		password: password,
		email: req.body.email
	});
	User.getOne(newUser.username, function (err, user) {
		if (!user) {
			req.flash('error','user is not exist');
			return res.redirect('back');
		}
		if (user.password != password) {
			req.flash('error','incorrect password');
			return res.redirect('back');
		}
		req.session.user = user;
		req.flash('success', 'login successfully!');
		res.redirect('/');
	});
});

/***********************登出***************/
router.get('/logout', function (req, res, next) {
	req.session.user = null;
	// req.flash('success', 'logout successfully!');
	res.redirect('/');
});

/***********************关注***************/
router.get('/follow/:username', checkLogin);
router.get('/follow/:username', function (req, res, next) {
	var date = new Date(),
		time = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" +date.getDate() + " " + date.getHours() + ":" + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
	var follow = {
		username : req.params.username,
		time : time
	};
	var newFollow = new Follow(req.session.user.username, follow);
	newFollow.save(function (err, follow) {
		if (err) {
			req.flash('error', err);
			return res.redirect('back');
		}
		Follow.getAll(req.session.user.username, function (err, follows) {
			console.log(JSON.stringify(follows));
			res.redirect('back');
		});
	});
});

/***********************取消关注***************/
router.get('/unfollow/:username', checkLogin);
router.get('/unfollow/:username', function (req, res, next) {
	var currentUser = req.session.user;
	Follow.remove(currentUser.username, req.params.username, function (err) {
		if (err) {
			req.flash('error', err);
			return res.redirect('back');
		}
		res.redirect('back');
	});
});
/***********************发表博文***************/
router.get('/post', checkLogin);
router.get('/post', function (req, res, next) {
	res.render('/', { 
		title:'post',
  		user: req.session.user,
  		success: req.flash('success').toString(),
  		error: req.flash('error').toString()
	});
});

router.post('/post', checkLogin);
router.post('/post', function (req, res, next) {
	var currentUser = req.session.user,
		newPost = new Post(currentUser.username, req.body.content, null ,null);
	newPost.save(function (err, post) {
		if (err) {
			req.flash('error', err);
			return res.redirect('/');
		}
		var posts = [];
		posts.push(post);
		var paths = path.join(__dirname, '../views') + '/index-body-center-article-blog.ejs';
		var blog = fs.readFileSync(paths, 'utf8');
		var html = ejs.render(blog, {
			user : req.session.user,
			posts : posts
		});
		res.json({
			html: html
		});
	});
});

/***********************转发***************/
router.post('/repost', checkLogin);
router.post('/repost', function (req, res, next) {
	var currentUser = req.session.user;
	var	newPost = new Post(currentUser.username, req.body.contentHTML,null , req.body.refer);
	newPost.save(function (err, post) {
		if (err) {
			req.flash('error', err);
			return res.redirect('/');
		}
		var posts = [];
		posts.push(post);
		var paths = path.join(__dirname, '../views') + '/index-body-center-article-blog.ejs';
		var blog = fs.readFileSync(paths, 'utf8');
		var html = ejs.render(blog, {
			user : req.session.user,
			posts : posts
		});
		res.json({
			html: html
		});
	});
});
/***********************上传***************/
router.get('/upload', checkLogin);
router.get('/upload', function (req, res, next) {
	res.render('upload', {
		title: 'Upload Documents...',
		user: req.session.user,
		success: req.flash('success').toString(),
		error: req.flash('error').toString()
	});
});

router.post('/upload', checkLogin);
router.post('/upload', function (req, res, next) {
	for (var i in req.files) {
		if (req.files[i].size === 0) {
			fs.unlinkSync(req.files[i].path);
			console.log('successfully removed an emtpy file!');
		} else {
			var target_path = './public/images/' + req.files[i].name;
			fs.renameSync(req.files[i].path, target_path);
			console.log('successfully renamed a file!');
		}
		console.log(req.files[i].path);
	}
	// req.flash('success', 'files has uploaded successful');
	res.json({
		success : 'files has uploaded successful' 
	});
});

/***********************博文评论***************/
router.get('/comment/:author/:day/:post_id', function (req, res, next) {
	Post.getOne(req.params.author, req.params.day, req.params.post_id, function (err, post) {
		if (err) {
			req.flash('error', err);
			return res.redirect('/');
		}
		var paths = path.join(__dirname, '../views') + '/index-body-center-article-comment.ejs';
		var comment = fs.readFileSync(paths, 'utf8');
		var html = ejs.render(comment,{
			post: post
		});
		res.json({
			html : html
		});
	});
});

router.post('/comment/:author/:day/:post_id', checkLogin);
router.post('/comment/:author/:day/:post_id', function (req, res, next) {
	var date = new Date(),
		time = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" +date.getDate() + " " + date.getHours() + ":" + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
	var comment = {
		author : req.session.user.username,
		time : time,
		content : req.body.content
	};
	var newComment = new Comment(req.params.author, req.params.day, req.params.post_id, comment);
	newComment.save(function (err, comment) {
		if (err) {
			req.flash('error', err);
			return res.redirect('back');
		}
		// req.flash('success','留言成功！');
		// res.redirect('back');
		var comments = [];
		comments.push(comment);
		var post = {
			comments : comments
		};
		var paths = path.join(__dirname, '../views') + '/index-body-center-article-comment.ejs';
		var comment = fs.readFileSync(paths, 'utf8');
		var html = ejs.render(comment,{
			post: post
		});
		res.json({
			html : html
		});
	});
});

/***********************更改博文***************/
router.get('/edit/:author/:day/:post_id', checkLogin);
router.get('/edit/:author/:day/:post_id', function (req, res, next) {
	var currentUser = req.session.user;
	Post.edit(currentUser.username, req.params.day, req.params.post_id, function (err, post) {
		if (err) {
			req.flash('error', err);
			return res.redirect('back');
		}
		res.render('edit', {
			title: 'edit',
			post: post,
			user: req.session.user,
			success: req.flash('success').toString(),
			error: req.flash('error').toString()
		});
	});
});

router.post('/edit/:author/:day/:post_id', checkLogin);
router.post('/edit/:author/:day/:post_id', function (req, res, next) {
	var currentUser = req.session.user;
	Post.update(currentUser.username, req.params.day, req.params.post_id, req.body.content, function (err) {
		var url = '/u/' + req.params.author + '/' + req.params.day + '/' + req.params.post_id;
		if (err) {
			req.flash('error', err);
			return res.redirect(url);
		}
		req.flash('success', 'update successfully!');
		res.redirect(url);    
	});
});

/***********************删除博文***************/
router.get('/remove/:author/:day/:post_id', checkLogin);
router.get('/remove/:author/:day/:post_id', function (req, res, next) {
	var currentUser = req.session.user;
	Post.remove(currentUser.username, req.params.day, req.params.post_id, function (err) {
		if (err) {
			//req.flash('error', err);
			return res.redirect('back');
		}
		// req.flash('success', 'remove successfully!');
		//res.redirect('/');
		res.send({ str : 'remove'});
	});
});

/***********************检查登录***************/
function checkLogin (req, res, next) {
	if (!req.session.user) {
		req.flash('error', 'You has not login.');
		return res.redirect('/');
	}
	next();
}

function checkNotLogin (req, res, next) {
	if (req.session.user) {
		req.flash('error', 'You has logined.');
		return res.redirect('back');
	}
	next();
}

module.exports = router;
